class flightSelectionPage
{
    get verifyFlightPageTitle()
    {
        return $("//h1[contains(text(),'Select Flights')]")
    }
    get continueButton()
    {
        return $("//div[@class='button-wrapper']")
    }
    clickContinueButton(flightPagetitle)
    {
       try {
           
        //this.verifyFlightPageTitle.waitForExist({ timeout: 80000 });
        if(this.verifyFlightPageTitle.getText().includes(flightPagetitle))
        {
            console.log("Reached flight selection page")
        }
        else {
            console.log("Not Reached flight selection page");
        }
         this.continueButton.scrollIntoView();
         this.continueButton.click();
       } catch (error) {
           console.log(error)
       }
       
        
    }
    
    
   
}
export default new flightSelectionPage();