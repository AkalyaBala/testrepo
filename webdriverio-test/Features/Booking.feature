Feature:flight only booking in stg Environment
This feature file contains all the steps needed to do flight only booking 

Background: 
  Given I launch the application in "stg" Environment
Scenario: 
   
   Then I close the banner
   Then I validate the text "Flight" is displayed
   Then I fill all the datas "LAS|BLI|round|2|4|1|0" in landing page
   Then I click search Button
   Then I verify flight page title "Select Flights" and click continue button
   Then I verify bundle page title "Select your bundle" and click continue button
   Then I verify getting aroun page title "Special Rental Car Deals" and click continue button
   Then I enter all details in travelerPage 
   Then I select seats from seat Page
   
   


 