
class travelerPage

{


    fillTravellerInfo(totalTraveler)
    {
      for(i=1;i<=totalTraveler;i++)
      {
         var firstName= $("(//input[@name='traveller[firstname]'])["+i+"]");
         firstName.clear();
         firstName.setValue("Test"+i);
         var lastName=$("(//input[@name='traveller[lastname]'])["+i+"]");
         lastName.clear();
         lastName.setValue("QA"+i);
         var gender=$("(//span[text()='Female'])["+i+"]");
         gender.click();
         var month=$("(//select[@name='dmy[m]'])["+i+"]");
         month.selectByVisibleText("Nov");
         var date=$("(//select[@name='dmy[d]'])["+i+"]");
         date.selectByVisibleText("14");
         var year=$("(//input[@name='dmy[y]'])["+i+"]");
         year.setValue("1997");
         var continueButton=$("//button[@class='continue']");
         continueButton.scrollIntoView();
         continueButton.click();

      }
    }
}
export default new travelerPage();