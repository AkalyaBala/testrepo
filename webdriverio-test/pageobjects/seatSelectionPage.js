class seatSelectionPage
{
 
    get returncontinueButton()
    {
        return $("//button[@class='continue']");
    }
    get continueButton()
    {
        return $("//button[@class='next-leg']");
    }
    get yes()
    {
        return $("//button[@class='yes_no_seats continue']");
    }
    selectSeat()
    {
    this.continueButton.scrollIntoView();
    this.continueButton.click();
    var yesDisplay=this.yes.isDisplayed();
    if(yesDisplay==true)
    {
        this.yes.click();
    }
    else{
        this.returncontinueButton.scrollIntoView();
        this.returncontinueButton.click();
        this.yes.click();
    }
    
  
    }
}
export default new seatSelectionPage();