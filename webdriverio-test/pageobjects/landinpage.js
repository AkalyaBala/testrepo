class landingpage
{
    
    get fromcity()
    {
        return $("//input[@placeholder='Departure City']")
    }
    get destinationCity()
    {
        return $("//input[@placeholder='Destination City']")
    }
    get tripType()
    {
        return $("one_way_radio")
    }
    get selectDepartureDate()
    {
        return $("//input[@name='from_date']")
    }
    get selectReturnDate()
    {
        return $("//input[@name='return_date']")
    }
    get roundTrip()
    {
        return $("//label[@for='round_trip_radio']")
    }
    get oneWay()
    {
        return $("//label[@for='one_way_radio']")
    }

    get calendarClick()
    {
        return $("datepicker-toggle")
    } 

    get calendarClick()
    {
        return $("datepicker-toggle")
    } 
    get NextMonthButton()
    {
        return $("//a[contains(@class,'ui-datepicker-next')]")
    }
    get CalenderMonth()
    
    {
        return $("//*[@class='ui-datepicker-month']")
    }

    get AvailDate()
    {
        return $$("//td[contains(@id,'ui-datepicker')]");
    }
    get AvailDateCheck()
    {
        return $("(//td[contains(@id,'ui-datepicker')])[1]");
    }
   get adultSelect()
   {
    return $("//select[@name='numb_of_adults']");
   }
   get childSelect()
   {
       return $("//select[@name='numb_of_children']");
   }
   get clickSearch()
   {
       return $("//div[@class='wpb_wrapper']//button[contains(text(),'Search')]");
   }
   get verifyFlightPageTitle()
    {
        return $("//h1[contains(text(),'Select Flights')]")
    }
    enterCityPair(sourceCity,destinationPlace)
    {
        this.fromcity.setValue(String(sourceCity));
        browser.keys('Enter');
        browser.pause(3000);
        this.destinationCity.setValue(String(destinationPlace));
        browser.keys('Enter');
        browser.pause(3000);
    }
    selectDate(days)
    {
        var currentdate = this.getSelectDay(days);
	
		var currentday = currentdate.split("/");
   var date = this.getSelectDay(days);
   console.log(date);
   var givendate = date.split("/");
   var givenMonth = parseInt(givendate[0]);
  var dates = parseInt(givendate[1]);
  var current_date = new Date();
  var startTime = current_date.getMilliseconds();
 let myMap = new Map();
 myMap.set("january", 1);
         myMap.set("february", 2);
         myMap.set("march", 3);
         myMap.set("april", 4);
         myMap.set("may", 5);
         myMap.set("june", 6);
         myMap.set("july", 7);
         myMap.set("august", 8);
         myMap.set("september", 9);
         myMap.set("october", 10);
         myMap.set("november", 11);
         myMap.set("december", 12);
        
         try {
             for (var loop = 0; loop < 4; loop++) {
          
              var month = givenMonth.toString();
                 if (myMap.get(this.CalenderMonth.getText().toLowerCase())== month) {
                     
                   
                    var ava=$("(//td[contains(@id,'ui-datepicker')])[1]");
                    var check=ava.isDisplayed();
                    
                          if (check==true) {
                             try {
                              var ele= $("//td[contains(@id,'ui-datepicker')]/a[text()='" + dates + "']");
                              ele.click();
                             console.log("Selected Travel Date is " + dates + "/" + givenMonth + "/" + givendate[2]);
                             var Depmonth = givenMonth;
                                 break;
                             } catch(error) {
                                 dates++;
                                 if (dates >= 30) {
                                     dates = 1;
                                     this.NextMonthButton.click();
                                     if (givenMonth == 12) {
                                         givenMonth = 1;
                                     } else {
                                         givenMonth++;
                                     }
                                 }
                             }
                         } else {
                             this.NextMonthButton.click();
                             if (givenMonth == 12) {
                                 givenMonth = 1;
                             } else {
                                 givenMonth = givenMonth + 1;
                             }
                         }
                     
                     break;
                 } else {
                     
                     if (myMap.get(this.CalenderMonth.getText().toLowerCase()) < givenMonth
 
                     && parseInt(givendate[2]) > parseInt(currentday[0])) {
 
                        this. NextMonthButton.click();
 
             } else {
                 givenMonth++;
                 browser.pause(3000)
             }
             browser.pause(3000)
         }
 
     }
 } catch(error){
     
    console.log("Error while selecting the Date pls refer the screenshot");
 }
 }
   selectTripTypeAndDate(tripType,number,returnDate)
   {
  if(tripType=="oneway")
  {
      this.oneWay.click();
      this.selectDepartureDate.click();
      browser.pause(3000);
      $("//td[contains(@id,'ui-datepicker')]/a[text()='20']").click();
      //this.selectDate(number)

  }
  else{
    
      this.roundTrip.click();
      this.selectDepartureDate.click();
    browser.pause(3000);
    this.selectDate(number);
      this.selectReturnDate.click();
      this.selectDate(returnDate)
  }
   } 
   selectAdultAndChild(adultsCount,childCount)
   {
    this.adultSelect.selectByVisibleText(adultsCount);
    this.childSelect.selectByVisibleText(childCount);
   }
   getSelectDay(days) {
    const date = new Date()
    date.setDate(new Date().getDate() +parseInt(days))
    return date.toLocaleDateString();
  }
    

   




 landingPageDetails(sourceCity,destinationPlace,tripType,number,returnDate,adultsCount,childCount)
   {
       this.enterCityPair(sourceCity,destinationPlace);
       this.selectTripTypeAndDate(tripType,number,returnDate);
       this.selectAdultAndChild(adultsCount,childCount);
      

   }
}
export default new landingpage();