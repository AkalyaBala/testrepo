import {Given,When,Then,And} from 'cucumber';
import landingpage from '../pageobjects/landinpage';
import flightSelectionPage from  '../pageobjects/flightSelectionPage';
import BundlePage from '../pageobjects/BundlePage';
import travelerPage from '../pageobjects/travelerPage';
import seatSelectionPage from '../pageobjects/seatSelectionPage';

var totalTraveler;

Given(/^I launch the application in "([^"]*)" Environment$/,function(env)
{
browser.url("https://www."+env+".allegiantair.com/");
browser.setTimeout({ 'pageLoad': 80000 });
browser.maximizeWindow();
var {setDefaultTimeout} = require('cucumber');

    setDefaultTimeout(80 * 1000);

});
Then(/^I close the banner$/,function () {
  var closeButton=  $("(//div[@id='modal-component']//following::button[@type='button'])[1]");
  closeButton.click();
});
Then(/^I validate the text "([^"]*)" is displayed$/, function(passingText) {
    browser.setTimeout({ 'pageLoad': 10000 });
    let flightText=$("(//div[@class='allegiant-menu-container']//a[text()='Flight'])[1]");
    var displayingText=flightText.getText();
    if(passingText==displayingText)
    {
        console.log("Successfully reached allegiant home page--->Pass");
    }
    else{
        console.log("Not Reached allegiant home page--->Fail")
    }
});
Then(/^I fill all the datas "([^"]*)" in landing page$/, function(details) {

   var a=details.split("|");
   var sourceCity=a[0];
   var destinationPlace=a[1];
   var tripType=a[2];
   var number=a[3];
   var returnDate=a[4];
   var adultsCount=a[5];
   var childCount=a[6];
   totalTraveler=adultsCount+childCount;

    landingpage.landingPageDetails(sourceCity,destinationPlace,tripType,number,returnDate,adultsCount,childCount);
});
Then(/^I click search Button$/, function() {
    try {
        $("//div[@class='wpb_wrapper']//button[contains(text(),'Search')]").click();
 
    } catch (error) {
        console.log(error);
    }
    
});
Then(/^I verify flight page title "([^"]*)" and click continue button$/, function(flightPagetitle)  {
  
    
flightSelectionPage.clickContinueButton(flightPagetitle);

});

Then(/^I verify bundle page title "([^"]*)" and click continue button$/, function(bundlePageTitle) {
    BundlePage.clickNoThanks(bundlePageTitle);

});

Then(/^I verify getting aroun page title (.*) and click continue button$/, function(gettingArounTitle){
    var displayedTitle=$("//div[@id='transport']//h1");
    if(displayedTitle.getText().includes(gettingArounTitle))
    {
        console.log("Reached getting around page");
    }
    else{
        console.log("Not Reached getting around page");
    }
     var noThanks=$("//a[contains(@class,'no-item-selected')]");
     noThanks.scrollIntoView();
     noThanks.click();


});

Then('I enter all details in travelerPage', function() {
    travelerPage.fillTravellerInfo(totalTraveler);

});
Then('I select seats from seat Page', function() {
    seatSelectionPage.selectSeat();
});