class BundlePage
{
    get noThanksclick()
    {
        return $("//a[contains(@class,'no-item-selected')]")
    }
    get bundleTitle()
    {
        return $("//*[text()='Select your bundle']")
    }
    clickNoThanks(bundlePageTitle)
    {
    
        try {
			
			if (this.bundleTitle.getText().includes(bundlePageTitle)) {
				
                console.log("<-----------Bundles page------------>");
                this.noThanksclick.scrollIntoView();
				this.noThanksclick.click();
			}
			
		} catch (error) {
			console.log("Bundles page is skipped");

		}
    }
}
export default new BundlePage();